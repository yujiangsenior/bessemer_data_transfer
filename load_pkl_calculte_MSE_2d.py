#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 17:14:01 2021

@author: yujiang
"""

import pickle
import pprint
import numpy as np
#file = open('/home/yujiang/output/rollout_test_0.pkl','rb')
# file = open('/ichec/home/users/yujiang/rollouts/Drum40case/rollout_test_0.pkl', 'rb')
file = open('rollout_0.pkl', 'rb')
data = pickle.load(file)
#pprint.pprint(data)
#print(type(data))
#print((data['predicted_rollout']).shape)
#print(data['ground_truth_rollout'])
#print(data['predicted_rollout'].shape)
file.close()

steps_t = data['predicted_rollout'].shape[0]
num_P = data['predicted_rollout'].shape[1]
n_dim = data['predicted_rollout'].shape[2]
PX_gt = []
PY_gt = []
PZ_gt = []
PX_pred = []
PY_pred = []
PZ_pred = []
for i in range(steps_t):
    if i == 0:
        PX_gt = data['ground_truth_rollout'][i, :, 0]
        PY_gt = data['ground_truth_rollout'][i, :, 1]
        #PZ_gt = data['ground_truth_rollout'][i, :, 2]
        PX_pred = data['predicted_rollout'][i, :, 0]
        PY_pred = data['predicted_rollout'][i, :, 1]
        #PZ_pred = data['predicted_rollout'][i, :, 2]
    else:
        PX_gt = np.vstack((PX_gt, data['ground_truth_rollout'][i, :, 0]))
        PY_gt = np.vstack((PY_gt, data['ground_truth_rollout'][i, :, 1]))
        #PZ_gt = np.vstack((PZ_gt, data['ground_truth_rollout'][i, :, 2]))
        PX_pred = np.vstack((PX_pred, data['predicted_rollout'][i, :, 0]))
        PY_pred = np.vstack((PY_pred, data['predicted_rollout'][i, :, 1]))
        #PZ_pred = np.vstack((PZ_pred, data['predicted_rollout'][i, :, 2]))

X_loss = (PX_gt - PX_pred) ** 2
Y_loss = (PY_gt - PY_pred) ** 2
#Z_loss = (PZ_gt - PZ_pred) ** 2

loss = np.stack((X_loss, Y_loss))
print(loss.mean())
