#!/bin/bash

#SBATCH -p v5_192
#SBATCH --nodes=1
#SBATCH --ntasks=6
#SBATCH --cpus-per-task=8
#SBATCH -t 600
source /public1/soft/modules/module.sh
module purge
module load anaconda
source activate /public1/home/scfa0201/env/torch_gns

for i in {1..6}; do
    cd 3DAoR0$i
    srun -N 1 -n 1 -c 8 python train_or_infer2.py &
    cd ..
done
wait
