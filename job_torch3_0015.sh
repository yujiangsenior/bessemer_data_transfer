#!/bin/sh

#SBATCH -p blcy 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --time=6:00:00
#SBATCH -e torchrun_3D_drum_edge16node21_r0015.error###must change the name if the task switches between 2 CMD.
#SBATCH -o torchrun_3D_drum_3region_r0015.log
#SBATCH --open-mode=append

module purge
module load apps/anaconda3
source activate /public/home/xdsc0395/env/torch_gns

CMD="python train_or_infer3.py"

cd $SLURM_SUBMIT_DIR

JOBID=$SLURM_JOB_ID

echo -e "JobID: $JOBID\n======"
echo "Time: `date`"
echo "Running on master node: `hostname`"
echo "Current directory: `pwd`"

#if [ "$SLURM_JOB_NODELIST" ]; then
        #! Create a machine file:
#        export NODEFILE=`generate_pbs_nodefile`
#        cat $NODEFILE | uniq > machine.file.$JOBID
#        echo -e "\nNodes allocated:\n================"
#        echo `cat machine.file.$JOBID | sed -e 's/\..*$//g'`
#fi
#echo -e "\nnumtasks=$numtasks, numnodes=$numnodes, mpi_tasks_per_node=$mpi_tasks_per_node (OMP_NUM_THREADS=$OMP_NUM_THREADS)"

echo -e "\nExecuting command:\n==================\n$CMD\n"
#! Start recording time and run liggghts
echo "The job ${JOBID}  ${SLURM_JOB_NAME} starts on $(date)" > time.log
date1=$(date +"%s")
#$CMD0
$CMD
echo "The job ${JOBID} finishes on $(data)" >>time.log
data2=$(data +"%s")
