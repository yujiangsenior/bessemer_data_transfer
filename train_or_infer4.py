### Introduction
#line 359-380, edge embedding added from 3 to 12 in 2d case;
#line 574-580, added, to load info when training continues from previous checkpoint
#line 633-638, evaluation invoked during training; line 650-658, call save_checkpoint() if train_best or eval_best; line 439-445, save() and load() in Simulator are changed
#line 523, eval_loss calculated; line 184, aggr changed to 'add' 
#to switch between 3d and 2d, choose between line 513&514, 679&680, 681-682&683-684
#to switch between GCN and GNN , switch commented block in Processor() in line 170
#in order to have the sampling_frequency as a parameter, line 46, 54, 487, 473 are added or changed accordingly

### How to switch between 3 modes? By setting line 40-43, and changing line 22-25 accordingly
#Training mode(resume): model_path is not None, is_train=True, the latter 2 value is combined always
#Training mode(from 0): model_path=None, is_train=True
#Evaluation mode: model_path is not None, is_train=False. Also change line 26

import os
import json
import pickle
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import MessagePassing, radius_graph, GENConv, DeepGCNLayer
from torch.nn import Linear, LayerNorm, ReLU
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime#yujiang, 22.01.06, added
import shutil
import glob

my_data_path = '../datasets/SandRamps'#yujiang, 22.01.06, added
my_model_path = '../models/SandRamps/' #make sure / is included, yujiang, 22.01.06, added
my_rollouts_path = '../rollouts/SandRamps'#yujiang, 22.01.06, added
topic = 'SandRamps_DoE_case4_'#yujiang, 22.01.15, added
my_rollouts_path_topic = os.path.join(my_rollouts_path, topic+'0') #yujiang, change '1' to 'any number' corresponding to model directory postfix
os.makedirs(my_model_path, exist_ok=True) #yujiang, 22.01.06, previously 'train_log'
os.makedirs(my_rollouts_path_topic, exist_ok=True)

default_connectivity_radius = 0.05 #yujiang, added
INPUT_SEQUENCE_LENGTH = 3
kinematic_particle_id = 3 #yujiang, this is the particle id that stands for fixed particles in the field
batch_size = 2
noise_std = 2e-3 #yujiang, 22.01.06, originally 6.7e-4
training_steps = int(1e4) #yujiang, 22.01.06, originally 2e7
log_steps = 5
eval_steps = 50
save_steps = 2000
diameter = 0.005 #yujiang, the diameter of particles in DEM, corresponding to line 333
device = None#'cuda', or 'cpu', or None, #yujiang
sample_n_steps = 1 # yujiang, sampling position every n steps from tfrecord

is_train = True#yujiang, 22.01.06, added
model_path = '../models/SandRamps/SandRamps_DoE_case4_2/SandRamps_DoE_case4_2264checkpoint_trainbest.pth.tar' #yujiang, if not None, then it goes to Training mode(resume) or Evaluation mode
#model_path = None # 'model425000.pth', or None

with open(os.path.join(my_data_path, 'metadata.json'), 'rt') as f:#yujiang, 22.01.06
    metadata = json.loads(f.read())
num_steps = metadata['sequence_length']//sample_n_steps - INPUT_SEQUENCE_LENGTH # yujiang, 2022.02.03 changed
normalization_stats = {
    'acceleration': {
        'mean':torch.FloatTensor(metadata['acc_mean']).to(device), 
        'std':torch.sqrt(torch.FloatTensor(metadata['acc_std'])**2 + noise_std**2).to(device),
    }, 
    'velocity': {
        'mean':torch.FloatTensor(metadata['vel_mean']).to(device), 
        'std':torch.sqrt(torch.FloatTensor(metadata['vel_std'])**2 + noise_std**2).to(device),
    }, 
}

def build_mlp(
    input_size,
    layer_sizes,
    output_size=None,
    output_activation=torch.nn.Identity,
    activation=torch.nn.ReLU,
):
    sizes = [input_size] + layer_sizes
    if output_size:
        sizes.append(output_size)

    layers = []
    for i in range(len(sizes) - 1):
        act = activation if i < len(sizes) - 2 else output_activation
        layers += [torch.nn.Linear(sizes[i], sizes[i + 1]), act()]
    return torch.nn.Sequential(*layers)

def time_diff(input_sequence):
    return input_sequence[:, 1:] - input_sequence[:, :-1]

def get_random_walk_noise_for_position_sequence(position_sequence, noise_std_last_step):
    """Returns random-walk noise in the velocity applied to the position."""
    velocity_sequence = time_diff(position_sequence)
    num_velocities = velocity_sequence.shape[1]
    velocity_sequence_noise = torch.randn(list(velocity_sequence.shape)) * (noise_std_last_step/num_velocities**0.5)

    velocity_sequence_noise = torch.cumsum(velocity_sequence_noise, dim=1)

    position_sequence_noise = torch.cat([
        torch.zeros_like(velocity_sequence_noise[:, 0:1]),
        torch.cumsum(velocity_sequence_noise, dim=1)], dim=1)

    return position_sequence_noise

def _read_metadata(data_path):
    with open(os.path.join(data_path, 'metadata.json'), 'rt') as fp:
        return json.loads(fp.read())

def save_checkpoint(state, eval_is_best, is_best, best_eval_path, best_train_path, LOG_DIR):
    if eval_is_best:
        torch.save(state, best_eval_path)
    if is_best:
        for infile in glob.glob(os.path.join(LOG_DIR, '*trainbest.pth.tar')):
            os.remove(infile)
        torch.save(state, best_train_path) #yujiang,  'shutil.copyfile(checkpoint_path, best_model_path)'

class Encoder(nn.Module):
    def __init__(
        self, 
        node_in, 
        node_out, 
        edge_in, 
        edge_out,
        mlp_num_layers,
        mlp_hidden_dim,
    ):
        super(Encoder, self).__init__()
        self.node_fn = nn.Sequential(*[build_mlp(node_in, [mlp_hidden_dim for _ in range(mlp_num_layers)], node_out), 
            nn.LayerNorm(node_out)])
        self.edge_fn = nn.Sequential(*[build_mlp(edge_in, [mlp_hidden_dim for _ in range(mlp_num_layers)], edge_out), 
            nn.LayerNorm(edge_out)])

    def forward(self, x, edge_index, e_features): # global_features
        # x: (E, node_in)
        # edge_index: (2, E)
        # e_features: (E, edge_in)
        return self.node_fn(x), self.edge_fn(e_features)

class InteractionNetwork(MessagePassing):
    def __init__(
        self, 
        node_in, 
        node_out, 
        edge_in, 
        edge_out,
        mlp_num_layers,
        mlp_hidden_dim,
    ):
        super(InteractionNetwork, self).__init__(aggr='add')
        self.node_fn = nn.Sequential(*[build_mlp(node_in+edge_out, [mlp_hidden_dim for _ in range(mlp_num_layers)], node_out), 
            nn.LayerNorm(node_out)])
        self.edge_fn = nn.Sequential(*[build_mlp(node_in+node_in+edge_in, [mlp_hidden_dim for _ in range(mlp_num_layers)], edge_out), 
            nn.LayerNorm(edge_out)])

    def forward(self, x, edge_index, e_features):
        # x: (E, node_in)
        # edge_index: (2, E)
        # e_features: (E, edge_in)
        x_residual = x
        e_features_residual = e_features
        x, e_features = self.propagate(edge_index=edge_index, x=x, e_features=e_features)
        return x+x_residual, e_features+e_features_residual

    def message(self, edge_index, x_i, x_j, e_features):
        e_features = torch.cat([x_i, x_j, e_features], dim=-1)
        e_features = self.edge_fn(e_features)
        return e_features

    def update(self, x_updated, x, e_features):
        # x_updated: (E, edge_out)
        # x: (E, node_in)
        x_updated = torch.cat([x_updated, x], dim=-1)
        x_updated = self.node_fn(x_updated)
        return x_updated, e_features

class Processor(MessagePassing):
    def __init__(
        self, 
        node_in, 
        node_out, 
        edge_in, 
        edge_out,
        num_message_passing_steps,
        mlp_num_layers,
        mlp_hidden_dim,
    ):
        super(Processor, self).__init__(aggr='add')#yujiang, changed from 'max'
        #following is the GCN/gcn type of Processor part, yujiang, 2022.02.03
#        self.layers = torch.nn.ModuleList()

#        for i in range(1, num_message_passing_steps + 1):
#            conv = GENConv(mlp_hidden_dim, mlp_hidden_dim, aggr='softmax',
#                           t=1.0, learn_t=True, num_layers=mlp_num_layers, norm='layer')
#            norm = LayerNorm(mlp_hidden_dim, elementwise_affine=True)
#            act = ReLU(inplace=True)

#            layer = DeepGCNLayer(conv, norm, act, block='res+', dropout=0.1,
#                                 ckpt_grad=i % 3)
#            self.layers.append(layer)
        #original, message passing gnn
        self.gnn_stacks = nn.ModuleList([
            InteractionNetwork(
                node_in=node_in, 
                node_out=node_out,
                edge_in=edge_in, 
                edge_out=edge_out,
                mlp_num_layers=mlp_num_layers,
                mlp_hidden_dim=mlp_hidden_dim,
            ) for _ in range(num_message_passing_steps)])

    def forward(self, x, edge_index, e_features):
        #following is the GCN part of forward
#        x = self.layers[0].conv(x, edge_index, e_features)
#        for layer in self.layers[1:]:
#            x = layer(x, edge_index, e_features)
#        x = self.layers[0].act(self.layers[0].norm(x))
#        x = F.dropout(x, p=0.1, training=self.training)
        #original, message passing gnn part of forward
        for gnn in self.gnn_stacks:
            x, e_features = gnn(x, edge_index, e_features)
        return x, e_features

class Decoder(nn.Module):
    def __init__(
        self, 
        node_in, 
        node_out,
        mlp_num_layers,
        mlp_hidden_dim,
    ):
        super(Decoder, self).__init__()
        self.node_fn = build_mlp(node_in, [mlp_hidden_dim for _ in range(mlp_num_layers)], node_out)

    def forward(self, x):
        # x: (E, node_in)
        return self.node_fn(x)

class EncodeProcessDecode(nn.Module):
    def __init__(
        self, 
        node_in,
        node_out,
        edge_in,
        latent_dim,
        num_message_passing_steps,
        mlp_num_layers,
        mlp_hidden_dim,
    ):
        super(EncodeProcessDecode, self).__init__()
        self._encoder = Encoder(
            node_in=node_in, 
            node_out=latent_dim,
            edge_in=edge_in, 
            edge_out=latent_dim,
            mlp_num_layers=mlp_num_layers,
            mlp_hidden_dim=mlp_hidden_dim,
        )
        self._processor = Processor(
            node_in=latent_dim, 
            node_out=latent_dim,
            edge_in=latent_dim, 
            edge_out=latent_dim,
            num_message_passing_steps=num_message_passing_steps,
            mlp_num_layers=mlp_num_layers,
            mlp_hidden_dim=mlp_hidden_dim,
        )
        self._decoder = Decoder(
            node_in=latent_dim,
            node_out=node_out,
            mlp_num_layers=mlp_num_layers,
            mlp_hidden_dim=mlp_hidden_dim,
        )

    def forward(self, x, edge_index, e_features):
        # x: (E, node_in)
        x, e_features = self._encoder(x, edge_index, e_features)
        x, e_features = self._processor(x, edge_index, e_features)
        x = self._decoder(x)
        return x

class Simulator(nn.Module):
    def __init__(
        self,
        particle_dimension,
        node_in,
        edge_in,
        latent_dim,
        num_message_passing_steps,
        mlp_num_layers,
        mlp_hidden_dim,
        connectivity_radius,
        boundaries,
        normalization_stats,
        num_particle_types,
        particle_type_embedding_size,
        device='cuda',
    ):
        super(Simulator, self).__init__()
        self._boundaries = boundaries
        self._connectivity_radius = connectivity_radius
        self._normalization_stats = normalization_stats
        self._num_particle_types = num_particle_types

        self._particle_type_embedding = nn.Embedding(num_particle_types, particle_type_embedding_size) # (9, 16)

        self._encode_process_decode = EncodeProcessDecode(
            node_in=node_in,
            node_out=particle_dimension,
            edge_in=edge_in,
            latent_dim=latent_dim,
            num_message_passing_steps=num_message_passing_steps,
            mlp_num_layers=mlp_num_layers,
            mlp_hidden_dim=mlp_hidden_dim,
        )

        self._device = device

    def forward(self):
        pass

    def _build_graph_from_raw(self, position_sequence, n_particles_per_example, particle_types):
        n_total_points = position_sequence.shape[0]
        most_recent_position = position_sequence[:, -1] # (n_nodes, 2)
        velocity_sequence = time_diff(position_sequence)
        # senders and receivers are integers of shape (E,)
        senders, receivers = self._compute_connectivity(most_recent_position, n_particles_per_example, self._connectivity_radius)
        node_features = []
        # Normalized velocity sequence, merging spatial an time axis.
        velocity_stats = self._normalization_stats["velocity"]
        normalized_velocity_sequence = (velocity_sequence - velocity_stats['mean']) / velocity_stats['std']
        flat_velocity_sequence = normalized_velocity_sequence.view(n_total_points, -1)
        node_features.append(flat_velocity_sequence)

        # Normalized clipped distances to lower and upper boundaries.
        # boundaries are an array of shape [num_dimensions, 2], where the second
        # axis, provides the lower/upper boundaries.
        boundaries = torch.tensor(self._boundaries, requires_grad=False).float().to(self._device)
        distance_to_lower_boundary = (most_recent_position - boundaries[:, 0][None])
        distance_to_upper_boundary = (boundaries[:, 1][None] - most_recent_position)
        distance_to_boundaries = torch.cat([distance_to_lower_boundary, distance_to_upper_boundary], dim=1)
        #normalized_clipped_distance_to_boundaries = torch.clamp(distance_to_boundaries / self._connectivity_radius, -1., 1.)
        normalized_clipped_distance_to_boundaries = distance_to_boundaries / self._connectivity_radius
        
        node_features.append(normalized_clipped_distance_to_boundaries)
        
        if self._num_particle_types > 1:
            particle_type_embeddings = self._particle_type_embedding(particle_types)
            node_features.append(particle_type_embeddings)

        # Collect edge features.
        edge_features = []

        # Relative displacement and distances normalized to radius
        # (E, 2)
        # normalized_relative_displacements = (
        #     torch.gather(most_recent_position, 0, senders) - torch.gather(most_recent_position, 0, receivers)
        # ) / self._connectivity_radius
        normalized_relative_displacements = (
            most_recent_position[senders, :] - most_recent_position[receivers, :]
        ) / self._connectivity_radius
        edge_features.append(normalized_relative_displacements)

        normalized_relative_distances = torch.norm(normalized_relative_displacements, dim=-1, keepdim=True)
        edge_features.append(normalized_relative_distances)
        
        #following is the (xi-xj)/(||xi-xj||2), 1/(||xi-xj||2), and two others added to the edge embedding, 22.01.24, yujiang
        edge_features.append(torch.div(1.0, torch.norm(normalized_relative_displacements, p = 1, dim=-1, keepdim=True)+0.0001))
        edge_features.append(torch.div(normalized_relative_displacements, normalized_relative_distances+0.0001))
        edge_features.append(torch.div(1.0, normalized_relative_distances+0.0001))
        edge_features.append(torch.div(normalized_relative_displacements, torch.norm(normalized_relative_displacements, p = 3, dim=-1, keepdim=True)+0.0001))

        #following is the overlap added to the edge embedding
        displacements = (
            most_recent_position[senders, :] - most_recent_position[receivers, :])
        overlap_distances = torch.norm(displacements, dim=-1, keepdim=True) - diameter #here diameter is set in the begining, it is the diameter of 1 particle in DEM dataset
        for i in range(len(overlap_distances)): #yujiang, added, 22.1.14, for adding overlap to the edge embedding
            if overlap_distances[i] > 1e-4:
                overlap_distances[i] = 0
        edge_features.append(overlap_distances/self._connectivity_radius) #yujiang, comment this if not needed

        #following is the relative velocity, added to edge_features
        most_recent_velocity = velocity_sequence[:, -1]#[? 2]
        velocity_stats = self._normalization_stats["velocity"]
        normalized_most_recent_velocity = (
                most_recent_velocity - velocity_stats["mean"])/velocity_stats["std"]
        normalized_relative_velocity = normalized_most_recent_velocity[senders, :] - normalized_most_recent_velocity[receivers, :]
        edge_features.append(normalized_relative_velocity) #yujiang, comment this if don't want to add this to edge feature

        return torch.cat(node_features, dim=-1), torch.stack([senders, receivers]), torch.cat(edge_features, dim=-1)

    def _compute_connectivity(self, node_features, n_particles_per_example, radius, add_self_edges=True):
        # handle batches. Default is 2 examples per batch

        # Specify examples id for particles/points
        batch_ids = torch.cat([torch.LongTensor([i for _ in range(n)]) for i, n in enumerate(n_particles_per_example)]).to(self._device)
        # radius = radius + 0.00001 # radius_graph takes r < radius not r <= radius
        edge_index = radius_graph(node_features, r=radius, batch=batch_ids, loop=add_self_edges) # (2, n_edges)
        receivers = edge_index[0, :]
        senders = edge_index[1, :]
        return receivers, senders

    def _decoder_postprocessor(self, normalized_acceleration, position_sequence):
        # The model produces the output in normalized space so we apply inverse
        # normalization.
        acceleration_stats = self._normalization_stats["acceleration"]
        acceleration = (
            normalized_acceleration * acceleration_stats['std']
        ) + acceleration_stats['mean']

        # Use an Euler integrator to go from acceleration to position, assuming
        # a dt=1 corresponding to the size of the finite difference.
        most_recent_position = position_sequence[:, -1]
        most_recent_velocity = most_recent_position - position_sequence[:, -2]

        new_velocity = most_recent_velocity + acceleration  # * dt = 1
        new_position = most_recent_position + new_velocity  # * dt = 1
        return new_position

    def predict_positions(self, current_positions, n_particles_per_example, particle_types):
        node_features, edge_index, e_features = self._build_graph_from_raw(current_positions, n_particles_per_example, particle_types)
        predicted_normalized_acceleration = self._encode_process_decode(node_features, edge_index, e_features)
        next_position = self._decoder_postprocessor(predicted_normalized_acceleration, current_positions)
        return next_position

    def predict_accelerations(self, next_position, position_sequence_noise, position_sequence, n_particles_per_example, particle_types):
        noisy_position_sequence = position_sequence + position_sequence_noise
        node_features, edge_index, e_features = self._build_graph_from_raw(noisy_position_sequence, n_particles_per_example, particle_types)
        predicted_normalized_acceleration = self._encode_process_decode(node_features, edge_index, e_features)
        next_position_adjusted = next_position + position_sequence_noise[:, -1]
        target_normalized_acceleration = self._inverse_decoder_postprocessor(next_position_adjusted, noisy_position_sequence)
        return predicted_normalized_acceleration, target_normalized_acceleration

    def _inverse_decoder_postprocessor(self, next_position, position_sequence):
        """Inverse of `_decoder_postprocessor`."""
        previous_position = position_sequence[:, -1]
        previous_velocity = previous_position - position_sequence[:, -2]
        next_velocity = next_position - previous_position
        acceleration = next_velocity - previous_velocity

        acceleration_stats = self._normalization_stats["acceleration"]
        normalized_acceleration = (acceleration - acceleration_stats['mean']) / acceleration_stats['std']
        return normalized_acceleration

    def save(self, state, LOG_DIR, path='model.pth'): #yujiang, comment: 'model.pth' is a default setting and will be used if nothing is passed into simulator.save()
        for infile in glob.glob(os.path.join(LOG_DIR, '*model.pth')):
            os.remove(infile) #yujiang, remove previous model.pth files stored, thus to only keep the latest model
        torch.save(state, path) #yujiang, changed from 'torch.save(self.state_dict(), path)'

    def load(self, path):
        self.load_state_dict(torch.load(path)['state_dict']) #yujiang, changed from 'self.load_state_dict(torch.load(path))'

def prepare_data_from_tfds(data_path=os.path.join(my_data_path, 'train.tfrecord'), is_rollout=False, batch_size=2):#yujiang, 22.01.06, previously 'data/train.tfrecord'
    import functools
    import tensorflow.compat.v1 as tf
    import tensorflow_datasets as tfds
    import reading_utils
    import tree
    from tfrecord.torch.dataset import TFRecordDataset
    def prepare_inputs(tensor_dict):
        pos = tensor_dict['position']
        pos = tf.transpose(pos, perm=[1, 0, 2])
        target_position = pos[:, -1]
        tensor_dict['position'] = pos[:, :-1]
        num_particles = tf.shape(pos)[0]
        tensor_dict['n_particles_per_example'] = num_particles[tf.newaxis]
        if 'step_context' in tensor_dict:
            tensor_dict['step_context'] = tensor_dict['step_context'][-2]
            tensor_dict['step_context'] = tensor_dict['step_context'][tf.newaxis]
        return tensor_dict, target_position
    def batch_concat(dataset, batch_size):
        windowed_ds = dataset.window(batch_size)
        initial_state = tree.map_structure(lambda spec: tf.zeros(shape=[0] + spec.shape.as_list()[1:], dtype=spec.dtype),dataset.element_spec)
        def reduce_window(initial_state, ds):
            return ds.reduce(initial_state, lambda x, y: tf.concat([x, y], axis=0))
        return windowed_ds.map(lambda *x: tree.map_structure(reduce_window, initial_state, x))
    def prepare_rollout_inputs(context, features):
        out_dict = {**context}
        pos = tf.transpose(features['position'], [1, 0, 2])
        target_position = pos[:, -1]
        out_dict['position'] = pos[:, :-1:sample_n_steps] # yujiang, 2022.02.03 changed
        out_dict['n_particles_per_example'] = [tf.shape(pos)[0]]
        if 'step_context' in features:
            out_dict['step_context'] = features['step_context']
        out_dict['is_trajectory'] = tf.constant([True], tf.bool)
        return out_dict, target_position

    metadata = _read_metadata(my_data_path) #yujiang, 22.01.06, previously 'data/'
    ds = tf.data.TFRecordDataset([data_path])
    ds = ds.map(functools.partial(reading_utils.parse_serialized_simulation_example, metadata=metadata))
    if is_rollout:
        ds = ds.map(prepare_rollout_inputs)
    else:    
        split_with_window = functools.partial(
            reading_utils.split_trajectory, sample_n_steps = sample_n_steps, 
            window_length=INPUT_SEQUENCE_LENGTH + 1) #yujiang, previously '6 + 1'
        ds = ds.flat_map(split_with_window)
        ds = ds.map(prepare_inputs)
        ds = ds.repeat()
        ds = ds.shuffle(512)
        ds = batch_concat(ds, batch_size)
    ds = tfds.as_numpy(ds)
#    for i in range(100): # clear screen
#        print() #yujiang, commented
    return ds

def eval_single_rollout(simulator, features, num_steps, device):
    initial_positions = features['position'][:, 0:INPUT_SEQUENCE_LENGTH]
    ground_truth_positions = features['position'][:, INPUT_SEQUENCE_LENGTH:]
    
    current_positions = initial_positions
    predictions = []
    for step in range(num_steps):
        next_position = simulator.predict_positions(
            current_positions,
            n_particles_per_example=features['n_particles_per_example'],
            particle_types=features['particle_type'],
        ) # (n_nodes, 2)
        # Update kinematic particles from prescribed trajectory.
        kinematic_mask = (features['particle_type'] == kinematic_particle_id).clone().detach().to(device) # yujiang, previously '== 3', now use kinematic_particle_id, defined in the beginning
        next_position_ground_truth = ground_truth_positions[:, step]
        kinematic_mask = kinematic_mask.bool()[:, None].expand(-1, 2)
        #kinematic_mask = kinematic_mask.bool()[:, None].expand(-1, 3) #yujiang, 3d case
        next_position = torch.where(kinematic_mask, next_position_ground_truth, next_position)
        predictions.append(next_position)
        current_positions = torch.cat([current_positions[:, 1:], next_position[:, None, :]], dim=1)
    predictions = torch.stack(predictions) # (time, n_nodes, 2)
    ground_truth_positions = ground_truth_positions.permute(1,0,2)
#    loss = (predictions - ground_truth_positions) ** 2 #yujiang, this evaluation loss is based on position differences for entire trajectory, 314 moments, e.g. shape: [314, 725, 2]
    loss = abs(predictions - ground_truth_positions)
    output_dict = {
        'initial_positions': initial_positions.permute(1,0,2).cpu().numpy(),
        'predicted_rollout': predictions.cpu().numpy(),
        'ground_truth_rollout': ground_truth_positions.cpu().numpy(),
        'particle_types': features['particle_type'].cpu().numpy(),
    }
    loss = loss.mean() #yujiang
    return output_dict, loss #yujiang

def eval_rollout(ds, simulator, num_steps, num_eval_steps=1, save_results=False, device='cuda'):
    eval_loss = []
    i = 0
    simulator.eval()
    with torch.no_grad():
        for example_i, (features, labels) in enumerate(ds):
            features['position'] = torch.tensor(features['position']).to(device) # (n_nodes, 600, 2)
            features['n_particles_per_example'] = torch.tensor(features['n_particles_per_example']).to(device)
            features['particle_type'] = torch.tensor(features['particle_type']).to(device)
            labels = torch.tensor(labels).to(device)
            example_rollout, loss = eval_single_rollout(simulator, features, num_steps, device)
            example_rollout['metadata'] = metadata
            eval_loss.append(loss)
            if save_results:
                print('No.'+str(i)+'validation trajectory')##yujiang, added
                example_rollout['metadata'] = metadata
                filename = f'rollout_{example_i}.pkl'
                filename = os.path.join(my_rollouts_path_topic, filename)
                with open(filename, 'wb') as f:
                    pickle.dump(example_rollout, f)
            i += 1
            if i >= num_eval_steps:
                break
    simulator.train()
    return sum(eval_loss)/len(eval_loss) #yujiang, originally 'return torch.stack(eval_loss).mean(0)'

def train(simulator):
    i = 0
    while os.path.isdir(my_model_path+topic+str(i)):#yujiang, 22.01.06, _topic added
        i += 1
    LOG_DIR = my_model_path+topic+str(i)+'/'#yujiang, 22.01.06, _topic added

    writer = SummaryWriter(LOG_DIR)
    best_loss = 1e2#yujiang, 22.1.9, added
    eval_best_loss = 1e3#yujiang, 22.1.16, added
    lr_init = 1e-4
    lr_min = 1e-6
    lr_decay = 0.1
    lr_decay_steps = int(5e6)
    lr_new = lr_init
    optimizer = torch.optim.Adam(simulator.parameters(), lr=lr_init)
    step = 0

    if model_path is not None: #yujiang, 22.1.19
#        lr_init = torch.load(model_path)['lr'] #yujiang, depends
#        optimizer = torch.optim.Adam(simulator.parameters(), lr=lr_init)
        optimizer.load_state_dict(torch.load(model_path)['optimizer'])
        step = torch.load(model_path)['step']
        best_loss = torch.load(model_path)['best_loss']
        eval_best_loss = torch.load(model_path)['eval_best_loss'] #yujiang, must use this

    ds = prepare_data_from_tfds(batch_size=batch_size)
    # ds_eval = prepare_data_from_tfds(data_path=os.path.join(my_data_path, 'valid.tfrecord'), is_rollout=True)#yujiang, 22.1.11, uncommented, modified, previously 'data/valid.tfrecord'

    try:
        for features, labels in ds:
            features['position'] = torch.tensor(features['position']).to(device)
            features['n_particles_per_example'] = torch.tensor(features['n_particles_per_example']).to(device)
            features['particle_type'] = torch.tensor(features['particle_type']).to(device)
            labels = torch.tensor(labels).to(device)

            sampled_noise = get_random_walk_noise_for_position_sequence(features['position'], noise_std_last_step=noise_std).to(device)
            non_kinematic_mask = (features['particle_type'] != kinematic_particle_id).clone().detach().to(device) # yujiang, previously '!= 3'
            sampled_noise *= non_kinematic_mask.view(-1, 1, 1)

            pred, target = simulator.predict_accelerations(
                next_position=labels, 
                position_sequence_noise=sampled_noise, 
                position_sequence=features['position'], 
                n_particles_per_example=features['n_particles_per_example'], 
                particle_types=features['particle_type'],
            )
#            loss = (pred - target) ** 2#e.g. [1592, 2], or [820, 2]..., 1592/820 stands for the number of particles for specific piece of trajectory
            loss = abs(pred - target)
            loss = loss.sum(dim=-1)#e.g. [1592, 1], or [820, 1]...
            num_non_kinematic = non_kinematic_mask.sum()

            loss = torch.where(non_kinematic_mask.bool(), loss, torch.zeros_like(loss))
            loss = loss.sum() / num_non_kinematic

            if step % log_steps == 0:
                writer.add_scalar("training_loss", loss, step)
                writer.add_scalar("lr", lr_new, step)

            optimizer.zero_grad()
            loss.backward() #calculate the gradients
            optimizer.step() #update the parameters

            lr_new = lr_init * (lr_decay ** (step/lr_decay_steps))
            for g in optimizer.param_groups:
                g['lr'] = lr_new

            step += 1
            print(f'Training step: {step}/{training_steps}. Loss: {loss}.', end="\r",)
            if step >= training_steps: #yujiang, 22.01.06, delete ==0
                break

            is_best = loss < best_loss #yujiang, 22.1.9, added
            best_loss = min(loss, best_loss) #yujiang, 22.1.9, added
            eval_is_best = False #yujiang, for each step, the eval_is_best is set to False, because it is not evaluated in the next 'if block' each step, so just to avoid entering into the final 'if block' each step after the eval_is_best was set to True

            if step % eval_steps == 0: #yujiang, 22.01.11, this block is uncommented
                ds_eval = prepare_data_from_tfds(data_path=os.path.join(my_data_path, 'valid.tfrecord'), is_rollout=True)#yujiang, 22.1.11, uncommented, modified, previously 'data/valid.tfrecord'
                eval_loss = eval_rollout(ds_eval, simulator, num_steps, num_eval_steps=1, device=device)
                writer.add_scalar("eval_loss", eval_loss, step)
                eval_is_best = eval_loss < eval_best_loss
                eval_best_loss = min(eval_loss, eval_best_loss)

            if step % save_steps == 0: #yujiang, changed from simulator.save(LOG_DIR+'model.pth')
                simulator.save({
                    'step': step + 1,
                    'state_dict': simulator.state_dict(),
                    'best_loss': best_loss,
                    'eval_best_loss': eval_best_loss, 
                    'lr': lr_new, 
                    'optimizer': optimizer.state_dict()
                }, LOG_DIR, LOG_DIR+str(step)+'model.pth')

            if eval_is_best or is_best: #yujiang, added, 22.1.12
                save_checkpoint({
                    'step': step + 1,
                    'state_dict': simulator.state_dict(),
                    'best_loss': best_loss,
                    'eval_best_loss': eval_best_loss, 
                    'lr': lr_new, 
                    'optimizer': optimizer.state_dict()
                }, eval_is_best, is_best, LOG_DIR+topic+str(step)+'checkpoint_evalbest.pth.tar', LOG_DIR+topic+str(step)+'checkpoint_trainbest.pth.tar', LOG_DIR)

    except KeyboardInterrupt:
        pass

    simulator.save({
        'step': step + 1,
        'state_dict': simulator.state_dict(),
        'best_loss': best_loss, 
        'eval_best_loss': eval_best_loss,
        'lr': lr_new, 
        'optimizer': optimizer.state_dict()
    }, LOG_DIR, LOG_DIR+str(step)+'model.pth') #yujiang, 22.01.06, previously no 'step', 2nd parameter 'LOG_DIR' is passed so as to let the save() do the delete work inside LOG_DIR

def infer(simulator):
    start2 = datetime.now()
    ds = prepare_data_from_tfds(data_path=os.path.join(my_data_path, 'test.tfrecord'), is_rollout=True) #yujiang, 22.01.06, previously 'data/valid.tfrecord'
    print("Finished Training ! ", "Duration time:", datetime.now()-start2) 
    eval_rollout(ds, simulator, num_steps=num_steps, num_eval_steps=5, save_results=True, device=device) #yujiang, num_eval_steps=5 is added
    print("Finished Training ! ", "Duration time:", datetime.now()-start2) 

if __name__ == '__main__':
    start = datetime.now() #yujiang, 22.01.06, added
    simulator = Simulator(
        particle_dimension=2,
        #particle_dimension=3, #yujiang, 3d case
        node_in=11, #yujiang, originally 30
        edge_in=12, #yujiang, originally 3, relative velocity is added into edge embedding, so we needs this
        #node_in=9, #yujiang, 3d case
        #edge_in=16, #yujiang, 3d case
        latent_dim=128,
        num_message_passing_steps=12, #yujiang, originally 10
        mlp_num_layers=2,
        mlp_hidden_dim=128,
        connectivity_radius=default_connectivity_radius, #yujiang, 22.1.11, modified
        boundaries=np.array(metadata['bounds']),
        normalization_stats=normalization_stats,
        num_particle_types=9, # yujiang, previously 9
        particle_type_embedding_size=3,
        device=device,
    )
    if model_path is not None:
        simulator.load(model_path)
    if device == 'cuda':
        simulator.cuda()
    if is_train: #yujiang, 22.01.06, added
        train(simulator)
    else:
        infer(simulator)
    print("Finished Training ! Duration time:", datetime.now()-start) #yujiang, 22.01.06, added
