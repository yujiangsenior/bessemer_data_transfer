#!/bin/bash
#Let's get some color
RED='\033[0;31m'
UBlue='\033[4;34m'
NC='\033[0m' # No Color

casePrefix=3DAoR #change here xizhong
j=01 # start case num
mkdir -p "$casePrefix"{01..06} #change here xizhong
seed1=aaaa #string to be replace
seed2=bbbb
seed3=cccc
seed4=dddd
seed5=eeee
seed6=ffff
#seed7=gggg
#seed8=hhhh
rep1=(5e-5 1e-4 3e-4 6e-4 1e-3 2e-3)
rep2=(	0.1	0.5	0.1	0.5	0.1	0.5	0.3	0.5	0.3	0.1	0.3	0.1	0.5	0.1	0.5	0.1	0.5	)
rep3=(	0.1	0.35	0.6	0.6	0.1	0.1	0.6	0.1	0.35	0.6	0.1	0.6	0.6	0.1	0.1	0.35	0.6	)
rep5=(	0.3	0.3	0.3	0.6	0.9	0.9	0.9	0.3	0.6	0.9	0.3	0.3	0.3	0.6	0.9	0.9	0.9	)
rep6=(	0.001	0.001	0.001	0.001	0.001	0.001	0.001	0.0505	0.0505	0.0505	0.1	0.1	0.1	0.1	0.1	0.1	0.1	)

#rep6=(0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.055 0.055 0.055 0.1 0.1 0.1 0.1 0.1 0.1 0.1)
#rep7=(0 0 0.6 0.6 0 0.6 0 0.6 0 0.6 0.6 0 0.6 0 0 0.6)
#rep8=(0.1 0.001 0.1 0.1 0.001 0.001 0.001 0.1 0.1 0.001 0.001 0.001 0.1 0.1 0.1 0.001)
init=4 #inital value
step=6 #multiplyer
j=$(( j-1 )) #so the array start with 0
for i in ./$casePrefix*
do 
  if [ -d "$i" ]
  then
      \cp -rf {train_or_infer2.py,reading_utils.py} $i
      cd $i      
#      seed=$(grep -i 'abc' in.liggghts | awk {'print $7'})
#      rep=$(python -c "print($step**($j)*$init)")
      rep=$(python -c "print($step*($j)+$init)")
      echo -e " ${RED}case $j:${NC} seed: $seed1 rep: ${rep1[$j]} ${rep2[$j]} ${rep3[$j]}"
      sed -i 's/'"$seed1"'/'"${rep1[$j]}"'/g' train_or_infer2.py
#      sed -i 's/'"$seed2"'/'"${rep2[$j]}"'/g' in.liggghts
#      sed -i 's/'"$seed3"'/'"${rep3[$j]}"'/g' in.liggghts
#      sed -i 's/'"$seed4"'/'"${rep4[$j]}"'/g' in.liggghts
#      sed -i 's/'"$seed5"'/'"${rep5[$j]}"'/g' in.liggghts
#      sed -i 's/'"$seed6"'/'"${rep6[$j]}"'/g' in.liggghts
#      k=$(( ${rep1[$j]}*8 ))
#      k=1 #change here xizhong #of cpus
#      sh /public1/home/scfa0201/tools/runOneJob.sh $k #run the job
#      bash runOneJob.sh $k
      j=$(( j+1 ))
      cd ..      
  fi
done

#find the keyword line and output $6 str
#grep -i 'pack seed' in.* | awk {'print $6'}
 
